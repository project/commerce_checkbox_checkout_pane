<?php

namespace Drupal\commerce_checkbox_checkout_pane\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneInterface;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the completion message pane.
 *
 * @CommerceCheckoutPane(
 *   id = "commerce_checkbox_checkout_pane",
 *   label = @Translation("Commerce Checkbox Checkout Pane"),
 *   admin_description = @Translation("Allows defining a custom checkbox checkout pane"),
 *   default_step = "order_information",
 * )
 */
class CheckboxPane extends CheckoutPaneBase implements CheckoutPaneInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'pane_title' => NULL,
      'checkbox_machine_name' => 'commerce_checkbox_checkout_pane',
      'checkbox_title' => NULL,
      'checkbox_description' => NULL,
      'checkbox_required' => FALSE,
      'checkbox_default' => FALSE,
      'error_required_text' => '%field is required.',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationSummary() {
    $paneTitle = $this->configuration['pane_title'];
    $checkboxMachineName = $this->configuration['checkbox_machine_name'];
    $checkboxLabel = $this->configuration['checkbox_title'];
    $checkboxDescription = $this->configuration['checkbox_description'];
    $checkboxRequired = $this->configuration['checkbox_required'];
    $checkboxDefault = $this->configuration['checkbox_default'];
    $errorRequiredText = $this->configuration['error_required_text'];

    $summary = '';
    if (!empty($paneTitle)) {
      $summary .= $this->t('Pane title: "@text"', ['@text' => $paneTitle]) . '<br>';
    }

    if (!empty($paneDescription)) {
      $summary .= $this->t('Pane description: "@text"', ['@text' => $paneDescription]) . '<br>';
    }

    if (!empty($checkboxMachineName)) {
      $summary .= $this->t('Checkbox machine name: "@text"', ['@text' => $checkboxMachineName]) . '<br>';
    }

    if (!empty($checkboxLabel)) {
      $summary .= $this->t('Checkbox label: "@text"', ['@text' => $checkboxLabel]) . '<br>';
    }

    if (!empty($checkboxDescription)) {
      $summary .= $this->t('Checkbox description: "@text"', ['@text' => $checkboxDescription]) . '<br>';
    }

    if (!empty($checkboxRequired)) {
      $summary .= $this->t('Checkbox required') . '<br>';
      if (!empty($errorRequiredText)) {
        $summary .= $this->t('Required text: "@text"', ['@text' => $errorRequiredText]) . '<br>';
      }
    }
    else {
      $summary .= $this->t('Checkbox optional') . '<br>';
    }

    if (!empty($checkboxDefault)) {
      $summary .= $this->t('Checkbox checked by default') . '<br>';
    }
    else {
      $summary .= $this->t('Checkbox unchecked by default') . '<br>';
    }

    return $summary;
  }

  /**
   * Machine name exists callback from the 'machine_name' form element.
   *
   * @see https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Render!Element!MachineName.php/class/MachineName/11.x
   *
   * @param string $value
   * @param object $element
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public static function machineNameExists($value, $element, FormStateInterface $form_state) {
    // We never check this.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['pane_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Pane title'),
      '#default_value' => $this->configuration['pane_title'],
      '#required' => TRUE,
    ];
    $form['checkbox_machine_name'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('Checkbox machine name'),
      '#default_value' => $this->configuration['checkbox_machine_name'],
      '#required' => TRUE,
      '#machine_name' => [
        'exists' => [
          $this,
          'machineNameExists',
        ],
      ],
    ];
    $form['checkbox_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Checkbox label'),
      '#default_value' => $this->configuration['checkbox_title'],
      '#required' => TRUE,
    ];
    $form['checkbox_description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Checkbox description'),
      '#default_value' => $this->configuration['checkbox_description'],
    ];
    $form['checkbox_required'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Checkbox required'),
      '#default_value' => $this->configuration['checkbox_required'],
    ];
    $form['checkbox_default'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Checkbox checked by default'),
      '#default_value' => $this->configuration['checkbox_default'],
    ];
    $form['error_required_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Form error required text'),
      '#default_value' => $this->configuration['error_required_text'],
      '#description' => $this->t('The error message to show if the checkbox is required, but unchecked. Default: "%field is required.". You can use %field for the field title.'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['pane_title'] = $values['pane_title'] ?? '';
      $this->configuration['checkbox_machine_name'] = $values['checkbox_machine_name'];
      $this->configuration['checkbox_title'] = $values['checkbox_title'] ?? '';
      $this->configuration['checkbox_description'] = $values['checkbox_description'] ?? '';
      $this->configuration['checkbox_required'] = $values['checkbox_required'] ?? FALSE;
      $this->configuration['checkbox_default'] = $values['checkbox_default'] ?? FALSE;
      $this->configuration['error_required_text'] = $values['error_required_text'] ?? '';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    $pane_form[Xss::filter($this->configuration['checkbox_machine_name'], [])] = [
      '#type' => 'checkbox',
      '#title' => Xss::filterAdmin($this->configuration['checkbox_title']),
      '#default_value' => !empty($this->configuration['checkbox_default']),
      '#description' => Xss::filterAdmin($this->configuration['checkbox_description']),
      '#required' => $this->configuration['checkbox_required'] ?? FALSE,
      '#weight' => $this->getWeight(),
    ];

    return $pane_form;
  }

  /**
   * {@inheritdoc}
   */
  public function validatePaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    $values = $form_state->getValue($pane_form['#parents']);
    $checkboxValue = $values[Xss::filter($this->configuration['checkbox_machine_name'], [])] ?? FALSE;
    if ($this->configuration['checkbox_required'] && empty($checkboxValue)) {
      $form_state->setError($pane_form, $this->t(Xss::filterAdmin($this->configuration['error_required_text']), ['%field' => $this->configuration['checkbox_title']]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitPaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    $checkboxMachineName = Xss::filter($this->configuration['checkbox_machine_name'], []);
    $values = $form_state->getValue($pane_form['#parents']);
    $checkboxValue = $values[$checkboxMachineName] ?? FALSE;
    $this->order->setData('commerce_checkbox_checkout_pane', [$checkboxMachineName => $checkboxValue]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDisplayLabel() {
    return $this->configuration['pane_title'] ?? $this->pluginDefinition['pane_title'] ?? '';
  }

}
